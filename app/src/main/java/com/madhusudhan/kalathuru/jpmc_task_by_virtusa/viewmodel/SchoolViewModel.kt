package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.SATScores
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val repository: SchoolRepository) : ViewModel() {
    // State for holding search results
    private val _searchResults: MutableStateFlow<List<School>?> = MutableStateFlow(emptyList())
    val searchResults: StateFlow<List<School>?> = _searchResults.asStateFlow()

    // State for holding SAT scores
    private val _satScores: MutableStateFlow<SATScores?> = MutableStateFlow(null)
    val satScores: StateFlow<SATScores?> = _satScores.asStateFlow()

    init {
        // Load initial data when the ViewModel is created
        loadInitialData()
    }

    /**
     * Load initial data from the repository when the ViewModel is created.
     */
    private fun loadInitialData() {
        viewModelScope.launch {
            // Call the repository to get the initial list of schools
            repository.initialAPICall()
            // TODO: Handle the results if needed
        }
    }

    /**
     * Search schools by name.
     *
     * @param schoolName The name of the school to search for.
     */
    fun searchSchoolByName(schoolName: String) {
        viewModelScope.launch {
            // Call the repository to search for schools by name
            repository.searchSchoolsByName(schoolName)
                .collect { results ->
                    _searchResults.value = results
                }
        }
    }
    /**
     * Get SAT scores for a specific school.
     *
     * @param dbn The DBN (District Borough Number) of the school.
     */
    fun getSatScores(dbn: String) {
        viewModelScope.launch {
            // Call the repository to get SAT scores for a specific school
            repository.getSchoolDetails(dbn)
                .collect { scores ->
                    _satScores.value = scores
                }
        }
    }
}
