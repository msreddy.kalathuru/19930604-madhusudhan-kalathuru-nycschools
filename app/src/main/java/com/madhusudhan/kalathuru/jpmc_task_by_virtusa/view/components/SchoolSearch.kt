package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.view.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.viewmodel.SchoolViewModel
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreen(
    viewModel: SchoolViewModel,
    onItemClick: (School) -> Unit
) {
    var searchQuery by remember { mutableStateOf("") }
    var filterQuery by remember { mutableStateOf("") }

    val schools by viewModel.searchResults.collectAsState()

    val coroutineScope = rememberCoroutineScope()

    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            OutlinedTextField(
                value = searchQuery,
                onValueChange = { query -> searchQuery = query },
                label = { Text("Search") },
                modifier = Modifier.weight(0.8f)
            )
            Button(
                onClick = {
                    coroutineScope.launch {
                        viewModel.searchSchoolByName(searchQuery)
                    }
                },
                modifier = Modifier.weight(0.2f)
            ) {
                Text("Search")
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        OutlinedTextField(
            value = filterQuery,
            onValueChange = { query -> filterQuery = query },
            label = { Text("Filter") },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(16.dp))
        schools?.let { SchoolList(it) }
    }
}

@Composable
fun SchoolList(schools: List<School>) {
    LazyColumn {
        items(schools) { school ->
            SchoolItem(school)
        }
    }
}

@Composable
fun SchoolItem(school: School) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
        ) {
            Text(text = school.school_name, fontWeight = FontWeight.Bold)
            Text(text = school.overview_paragraph, color = Color.Gray)
        }
    }
}






