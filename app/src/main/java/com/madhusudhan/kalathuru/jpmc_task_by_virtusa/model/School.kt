package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// An entity class representing a school.
@Entity(tableName = "school_table")
data class School(
    // Primary key representing the unique identifier of the school (DBN).
    @PrimaryKey
    @ColumnInfo(name = "dbn")
    val dbn: String,

    // The name of the school.
    @ColumnInfo(name = "school_name")
    val school_name: String,

    // Overview paragraph describing the school.
    @ColumnInfo(name = "overview_paragraph")
    val overview_paragraph: String,

    // Phone number of the school.
    @ColumnInfo(name = "phone_number")
    val phone_number: String,

    // Email address of the school.
    @ColumnInfo(name = "school_email")
    val school_email: String,

    // Website URL of the school.
    @ColumnInfo(name = "website")
    val website: String,

    // Total number of students in the school.
    @ColumnInfo(name = "total_students")
    val total_students: String,

    // Primary address line of the school.
    @ColumnInfo(name = "primary_address_line_1")
    val primary_address_line_1: String,

    // City where the school is located.
    @ColumnInfo(name = "city")
    val city: String,

    // ZIP code of the school's location.
    @ColumnInfo(name = "zip")
    val zip: String,

    // Latitude coordinate of the school's location.
    @ColumnInfo(name = "latitude")
    val latitude: String,

    // Longitude coordinate of the school's location.
    @ColumnInfo(name = "longitude")
    val longitude: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        dbn = parcel.readString()!!,
        school_name = parcel.readString()!!,
        overview_paragraph = parcel.readString()!!,
        phone_number = parcel.readString()!!,
        school_email = parcel.readString()!!,
        website = parcel.readString()!!,
        total_students = parcel.readString()!!,
        primary_address_line_1 = parcel.readString()!!,
        city = parcel.readString()!!,
        zip = parcel.readString()!!,
        latitude = parcel.readString()!!,
        longitude = parcel.readString()!!
    )

    // Write object data to a Parcel.
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(dbn)
        dest.writeString(school_name)
        dest.writeString(overview_paragraph)
        dest.writeString(phone_number)
        dest.writeString(school_email)
        dest.writeString(website)
        dest.writeString(total_students)
        dest.writeString(primary_address_line_1)
        dest.writeString(city)
        dest.writeString(zip)
        dest.writeString(latitude)
        dest.writeString(longitude)
    }

    // Describe the kinds of special objects contained in the Parcelable.
    override fun describeContents(): Int {
        return 0
    }

    // A companion object that implements Parcelable.Creator interface.
    companion object CREATOR : Parcelable.Creator<School> {
        // Create a School object from a Parcel.
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        // Create an array of School objects of the given size.
        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}
