package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SchoolApplication : Application()