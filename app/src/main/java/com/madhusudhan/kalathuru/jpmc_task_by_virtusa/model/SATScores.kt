package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

// An entity class representing SAT scores data for a school.
@Entity(tableName = "sat_table")
data class SATScores(
    // Primary key representing the unique identifier of the school (DBN).
    @PrimaryKey
    @ColumnInfo(name = "dbn")
    val dbn: String,

    // The name of the school.
    @ColumnInfo(name = "school_name")
    val school_name: String,

    // The number of students who took the SAT test.
    @ColumnInfo(name = "num_of_sat_test_takers")
    val num_of_sat_test_takers: String,

    // The average score for critical reading section of the SAT.
    @ColumnInfo(name = "sat_critical_reading_avg_score")
    val sat_critical_reading_avg_score: String,

    // The average score for math section of the SAT.
    @ColumnInfo(name = "sat_math_avg_score")
    val sat_math_avg_score: String,

    // The average score for writing section of the SAT.
    @ColumnInfo(name = "sat_writing_avg_score")
    val sat_writing_avg_score: String
)
