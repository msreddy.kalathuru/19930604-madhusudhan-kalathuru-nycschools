package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.repository

import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao.SATScoresDao
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao.SchoolsDao
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.remote.SchoolAPIService
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.SATScores
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Repository class responsible for managing data interactions between the local database and remote API.
 */
class SchoolRepository @Inject constructor(
    private val schoolsDao: SchoolsDao,
    private val satScoresDao: SATScoresDao,
    private val schoolService: SchoolAPIService
) {

    // Remote Operations

    /**
     * Makes an initial API call to fetch a list of schools and their SAT scores,
     * then inserts the data into the local database.
     *
     * @return List of schools retrieved from the API.
     */
    suspend fun initialAPICall() {
        val schoolsList = schoolService.getSchoolsList()
        val satScoreList = schoolService.getSATScoreList()
        if (schoolsList.isNotEmpty()) {
            insertSchools(schoolsList)
        }
        if (satScoreList.isNotEmpty()) {
            insertSATScores(satScoreList)
        }
    }

    // Local Operations - School Object Operations
    /**
     * Inserts a list of schools into the local database.
     *
     * @param schools List of School objects to be inserted.
     */
    private suspend fun insertSchools(schools: List<School>) {
        schoolsDao.insertSchools(schools)
    }

    /**
     * Retrieves all schools from the local database.
     *
     * @return Flow emitting a list of schools.
     */
    fun getAllSchools(): Flow<List<School>> {
        return schoolsDao.getAllSchools()
    }

    /**
     * Retrieves schools matching a search query from the local database.
     *
     * @param query The search query to filter schools by name.
     * @return Flow emitting a list of schools matching the search query.
     */
    fun searchSchoolsByName(query: String): Flow<List<School>> {
        return schoolsDao.getSchoolsFiltered(query)
    }

    // Local Operations - Score Object Operations
    /**
     * Inserts a list of SAT scores into the local database.
     *
     * @param satScores List of SATScores objects to be inserted.
     */
    private suspend fun insertSATScores(satScores: List<SATScores>) {
        satScoresDao.insertScores(satScores)
    }

    /**
     * Retrieves SAT scores for a specific school from the local database.
     *
     * @param query The unique identification code of the school.
     * @return Flow emitting SAT scores for the specified school.
     */
    fun getSchoolDetails(query: String): Flow<SATScores> {
        return satScoresDao.getScore(query)
    }
}
