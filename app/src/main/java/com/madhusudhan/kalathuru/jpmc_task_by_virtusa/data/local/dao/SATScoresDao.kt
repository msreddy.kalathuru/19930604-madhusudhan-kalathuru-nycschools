package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.SATScores
import kotlinx.coroutines.flow.Flow

@Dao
interface SATScoresDao {

    /**
     * Insert a list of SAT scores into the database.
     * If a score already exists, it will be ignored.
     *
     * @param scores List of SAT scores to insert or ignore.
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertScores(scores: List<SATScores>)

    /**
     * Delete all SAT scores from the table.
     */
    @Query("DELETE FROM sat_table")
    suspend fun deleteAll()

    /**
     * Retrieve the SAT scores for a specific school using its DBN.
     *
     * @param schoolDBN The DBN (District Borough Number) of the school.
     * @return Flow containing the SAT scores for the school.
     */
    @Transaction
    @Query("SELECT * FROM sat_table where dbn = :schoolDBN")
    fun getScore(schoolDBN: String): Flow<SATScores>

    /**
     * Retrieve all SAT scores from the table.
     * Scores are ordered by school name in ascending order.
     *
     * @return Flow containing a list of all SAT scores.
     */
    @Transaction
    @Query("SELECT * FROM sat_table  ORDER BY school_name ASC")
    fun getAllScores(): Flow<List<SATScores>>
}
