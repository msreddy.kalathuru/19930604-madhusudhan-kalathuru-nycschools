package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.di

import SchoolsDatabase
import android.content.Context
import androidx.room.Room
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao.SATScoresDao
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao.SchoolsDao
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.remote.SchoolAPIService
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.repository.SchoolRepository
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Dagger Hilt module providing dependencies for the application.
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    /**
     * Provides the SchoolsDatabase instance.
     */
    @Provides
    fun provideSchoolDatabase(context: Context): SchoolsDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            SchoolsDatabase::class.java,
            "nyc_school_database"
        ).build()
    }

    /**
     * Provides the SchoolsDao instance.
     */
    @Provides
    fun provideSchoolsDao(database: SchoolsDatabase): SchoolsDao {
        return database.schoolsDao()
    }

    /**
     * Provides the SATScoresDao instance.
     */
    @Provides
    fun provideSATScoresDao(database: SchoolsDatabase): SATScoresDao {
        return database.satScoresDao()
    }

    /**
     * Provides the SchoolRepository instance.
     */
    @Provides
    fun provideSchoolRepository(
        schoolsDao: SchoolsDao,
        satScoresDao: SATScoresDao,
        schoolService: SchoolAPIService
    ): SchoolRepository {
        return SchoolRepository(schoolsDao, satScoresDao, schoolService)
    }

    /**
     * Provides the SchoolAPIService instance using Retrofit.
     */
    @Provides
    fun provideApiInterface(): SchoolAPIService {
        return Retrofit.Builder().baseUrl(Constants.BASE_API_URL)
            .addConverterFactory(MoshiConverterFactory.create()).build()
            .create(SchoolAPIService::class.java)
    }
}
