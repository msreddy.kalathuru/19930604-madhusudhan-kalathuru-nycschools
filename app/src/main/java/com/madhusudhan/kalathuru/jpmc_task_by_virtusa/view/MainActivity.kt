package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.view.theme.JPMC_Task_29_08_2023Theme
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JPMC_Task_29_08_2023Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                   //Call SearchScreen
                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreen(
    viewModel: SchoolViewModel,
    onItemClick: (School) -> Unit
) {
    var searchQuery by remember { mutableStateOf("") }
    var filterQuery by remember { mutableStateOf("") }

    val schools by viewModel.searchResults.collectAsState()

    val coroutineScope = rememberCoroutineScope()

    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            OutlinedTextField(
                value = searchQuery,
                onValueChange = { query -> searchQuery = query },
                label = { Text("Search") },
                modifier = Modifier.weight(0.8f)
            )
            Button(
                onClick = {
                    coroutineScope.launch {
                        viewModel.searchSchoolByName(searchQuery)
                    }
                },
                modifier = Modifier.weight(0.2f)
            ) {
                Text("Search")
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        OutlinedTextField(
            value = filterQuery,
            onValueChange = { query -> filterQuery = query },
            label = { Text("Filter") },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(16.dp))
        schools?.let { SchoolList(it) }
    }
}

@Composable
fun SchoolList(schools: List<School>) {
    LazyColumn {
        items(schools) { school ->
            SchoolItem(school)
        }
    }
}

@Composable
fun SchoolItem(school: School) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
        ) {
            Text(text = school.school_name, fontWeight = FontWeight.Bold)
            Text(text = school.overview_paragraph, color = Color.Gray)
        }
    }
}
