package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import kotlinx.coroutines.flow.Flow

@Dao
interface SchoolsDao {

    /**
     * Insert a list of schools into the database.
     * If a school already exists, it will be replaced.
     *
     * @param schools List of schools to insert or replace.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchools(schools: List<School>)

    /**
     * Retrieve all schools from the database.
     * Schools are ordered by school name in ascending order.
     *
     * @return Flow containing a list of schools.
     */
    @Transaction
    @Query("SELECT * FROM school_table ORDER BY school_name ASC")
    fun getAllSchools(): Flow<List<School>>

    /**
     * Search for schools by name using a query.
     * Filtered schools are ordered by school name in ascending order.
     *
     * @param searchString The search query for school names.
     * @return Flow containing a list of filtered schools.
     */
    @Transaction
    @Query("SELECT * FROM school_table where school_name like :searchString ORDER BY school_name ASC")
    fun getSchoolsFiltered(searchString: String?): Flow<List<School>>

}

