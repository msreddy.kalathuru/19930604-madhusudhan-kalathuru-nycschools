import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao.SATScoresDao
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.local.dao.SchoolsDao
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.SATScores
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Room Database class for managing local data storage.
 */
@Database(entities = [School::class, SATScores::class], version = 1)
abstract class SchoolsDatabase : RoomDatabase() {
    abstract fun schoolsDao(): SchoolsDao
    abstract fun satScoresDao(): SATScoresDao

    /**
     * Dagger module responsible for providing instances of the Room database and DAOs.
     */
    @Module
    @InstallIn(SingletonComponent::class)
    object DatabaseModule {
        /**
         * Provides an instance of the Room database.
         *
         * @param context The application context.
         * @return SchoolsDatabase instance.
         */
        @Provides
        @Singleton
        fun provideDatabase(@ApplicationContext context: Context): SchoolsDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                SchoolsDatabase::class.java,
                "nyc_school_database"
            ).build()
        }

        /**
         * Provides an instance of the SchoolsDao.
         *
         * @param database The Room database instance.
         * @return SchoolsDao instance.
         */
        @Provides
        fun provideSchoolsDao(database: SchoolsDatabase): SchoolsDao {
            return database.schoolsDao()
        }

        /**
         * Provides an instance of the SATScoresDao.
         *
         * @param database The Room database instance.
         * @return SATScoresDao instance.
         */
        @Provides
        fun provideSATScoresDao(database: SchoolsDatabase): SATScoresDao {
            return database.satScoresDao()
        }
    }
}
