package com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.remote

import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.SATScores
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.model.School
import retrofit2.http.GET

/**
 * Retrofit interface for defining API endpoints related to schools and SAT scores.
 */
interface SchoolAPIService {
    /**
     * Fetches a list of schools from the API.
     *
     * @return List of School objects representing schools' basic information.
     */
    @GET("/s3k6-pzi2.json")
    suspend fun getSchoolsList(): List<School>

    /**
     * Fetches a list of SAT scores from the API.
     *
     * @return List of SATScores objects representing schools' SAT score information.
     */
    @GET("/f9bf-2cp4.json")
    suspend fun getSATScoreList(): List<SATScores>
}
