import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.data.repository.SchoolRepository
import com.madhusudhan.kalathuru.jpmc_task_by_virtusa.viewmodel.SchoolViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@Suppress("DEPRECATION")
@ExperimentalCoroutinesApi
class SchoolViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockRepository: SchoolRepository

    private lateinit var viewModel: SchoolViewModel

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = SchoolViewModel(mockRepository)
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun `loadInitialData() should call initialAPICall() in repository`() =
        testScope.runBlockingTest {

        }

    @Test
    fun `searchSchoolByName() should update searchResults`() = testScope.runBlockingTest {


    }

    @Test
    fun `getSatScores() should update satScores`() = testScope.runBlockingTest {

    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

}
